$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});	

	//CARROSSEL DE DESTAQUE
	$("#carrosselSalas").owlCarousel({
		items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});

	//BOTÕES DO CARROSSEL SALA HOME
	var carrossel_destaque = $("#carrosselSalas").data('owlCarousel');
	$('.carrosselSalasEsquerda').click(function(){ carrossel_destaque.prev(); });
	$('.carrosselSalasDireita').click(function(){ carrossel_destaque.next(); });

	//CARROSSEL DE DETALHES
	$("#carrosselDetalhes").owlCarousel({
		items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});

	//BOTÕES DO CARROSSEL SALA HOME
	var carrossel_salas = $("#carrosselDetalhes").data('owlCarousel');
	$('.carrosselDetalhesEsquerda').click(function(){ carrossel_salas.prev(); });
	$('.carrosselDetalhesDireita').click(function(){ carrossel_salas.next(); });

	$(window).bind('scroll', function () {
		       
		var alturaScroll = $(window).scrollTop()
		if(alturaScroll > 300){
		   $(".topo").addClass('thin');
		}
		else{
		   $(".topo").removeClass('thin');
		}
	});

	$(".abrirModalContrate").click(function(e){
        event.preventDefault();
        $(".lesto-fale-conosco.contrate").addClass("abrirModalContrate");
        $("body").addClass("travarScroll");
    });

    $(".fecharModal").click(function(e){
        $(".lesto-fale-conosco.contrate").removeClass("abrirModalContrate");
        $("body").removeClass("travarScroll");
    })

    $(".abrirModalDetalhes").click(function(e){
        event.preventDefault();
        $(".sala-detalhes").addClass("abrirModalDetalhes");
        $("body").addClass("travarScroll");
    });

    $(".fecharModal").click(function(e){
        $(".sala-detalhes").removeClass("abrirModalDetalhes");
        $("body").removeClass("travarScroll");
    })
		
});